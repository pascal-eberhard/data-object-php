<?php declare(strict_types=1);

/*
 * This file is part of the data-object-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE.md
 */

namespace PEPrograms\DataObject\UnitTests;

use PEPrograms\DataObject;
use PEPrograms\Utils\ClassAndObject\With\GetInstanceSelf;
use PEPrograms\Utils\ClassAndObject\With\ToArray;

/**
 * Some tools for data object unit tests
 *
 * @copyright 2020 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 */
final class Utils implements GetInstanceSelf\TheInterface
{

    /**
     * @return self
     */
    public static function get()
    {
        return new self();
    }

    /**
     * Create ::toArray() fail message suffix
     *
     * @param DataObject\TheInterface $data Must implement ToArrayInterface
     * @return string
     * @see \PEPrograms\Utils\ClassAndObject\With\ToArray\TheInterface
     */
    public function buildToArrayMessageSuffix(DataObject\TheInterface $data): string
    {
        if (!($data instanceof ToArray\TheInterface)) {
            throw new \InvalidArgumentException('Missing interface for $data ' . ToArray\TheInterface::class);
        }

        return '::toArray() (var export): ' . \var_export($data->toArray(), true);
    }
}
