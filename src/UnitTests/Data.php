<?php declare(strict_types=1);

/*
 * This file is part of the data-object-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE.md
 */

namespace PEPrograms\DataObject\UnitTests;

use PEPrograms\DataObject;
use PEPrograms\Utils\ClassAndObject;

/**
 * Simple test data object for unit tests
 *
 * @copyright 2020 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 */
class Data implements DataObject\TheInterface, ClassAndObject\With\ToArray\TheInterface
{

    /**
     * @var bool
     */
    private $flag = true;

    /**
     * Get flag
     *
     * @return bool
     */
    public function flag(): bool
    {
        return $this->flag;
    }

    /**
     * Set flag
     *
     * @param bool $flag
     * @return $this
     */
    public function flagSet(bool $flag)
    {
        $this->flag = $flag;

        return $this;
    }

    /**
     * @var string
     */
    private $label = '';

    /**
     * Get label
     *
     * @return string
     */
    public function label(): string
    {
        return $this->label;
    }

    /**
     * Set label
     *
     * @param string $label
     * @return $this
     */
    public function labelSet(string $label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * To array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'flag' => $this->flag,
            'label' => $this->label,
        ];
    }
}
