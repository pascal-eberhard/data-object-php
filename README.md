# Data objects

## Description

Some data object classes and tools.

### [Change log](resources/docs/changelog.md)

### License

The MIT License, see [License File](LICENSE.md).

## Installation

Via composer:

```bash
composer require data-object-php
```
