# Change log

## Version 1.1

```yaml
checks:
  # @todo Currently out of build time
  bitBucket: ""
  local:
    composerChecksPassed: true
    php: "7.2.1"
    operatingSystem:
      extry: "Git bash"
      name: "Windows 7"
repository:
  commit: "https://bitbucket.org/pascal-eberhard/data-object-php/commits/a8296a06198820f1644464576e9b253f89b358b4"
```

Deleted previous versions.
Change from three part to two part version number.
