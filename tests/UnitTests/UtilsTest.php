<?php declare(strict_types=1);

/*
 * This file is part of the data-object-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE.md
 */

namespace PEPrograms\DataObject\Tests\UnitTests;

use PEPrograms\DataObject\UnitTests;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \PEPrograms\DataObject\UnitTests\Utils
 * @copyright 2020 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * Shell: (vendor/bin/phpunit tests/UnitTests/UtilsTest.php)
 * @ \b, else all tests matching "testX*" would be executed
 */
class UtilsTest extends TestCase
{
    /**
     * @covers ::buildToArrayMessageSuffix
     * Shell: (vendor/bin/phpunit tests/UnitTests/UtilsTest.php --filter '/::testDefault\b/')
     * @ \b, else all tests matching "testX*" would be executed
     */
    public function testDefault()
    {
        $data = (new UnitTests\Data())
            ->flagSet(true)
            ->labelSet('Some label')
            ;
        $result = UnitTests\Utils::get()->buildToArrayMessageSuffix($data);
        $this->assertNotEmpty($result, $result);
    }
}
